#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ROWS 30
#define COLS 30

#define HERO1_SYMBOL '@'
#define HERO2_SYMBOL '%'
#define FOOD_SYMBOL '$'
#define BODY_SYMBOL '&'

struct Velocities {
    int dx;
    int dy;
};


struct Food {
    int x;
    int y;
    char symbol;
};

struct Element {
    int x;
    int y;

    int x_prev;
    int y_prev;
};


struct Player {
    struct Element body[BUFSIZ];
    size_t length;
    size_t player_number;
};

struct Game {
    char board[COLS][ROWS];
    struct Food food;
};

int generate_random_in_range(int min, int max) {
    int range = max - min + 1;
    return rand() % range + min;
}


void update_food_cords(struct Game *game) {
    int x, y;
    do {
        x = generate_random_in_range(0, COLS - 1);
        y = generate_random_in_range(0, ROWS - 1);
    } while (game->board[x][y] != ' ');

    game->food.x = x;
    game->food.y = y;

}

void generateFood(struct Game *game) {
    update_food_cords(game);
    game->food.symbol = FOOD_SYMBOL;
}

void init_player(struct Player *player, size_t player_num) {
    player->body[0].x = generate_random_in_range(0, COLS);
    player->body[0].y = generate_random_in_range(0, ROWS);

    player->body[0].x_prev = 0;
    player->body[0].y_prev = 0;
    player->length = 1;
    player->player_number = player_num;
}


void update_field_player(struct Game *game, struct Player player) {
    if (player.player_number != 1 && player.player_number != 2) {
        fprintf(stderr, "error in updating %zu", player.player_number);
        exit(1);
    }

    for (size_t i = 0; i < player.length; i++) {
        char symbol;
        if (i == 0) {
            if (player.player_number == 1) {
                symbol = HERO1_SYMBOL;
            }
            if (player.player_number == 2) {
                symbol = HERO2_SYMBOL;
            }
        } else {
            symbol = BODY_SYMBOL;
        }

        game->board[player.body[i].x][player.body[i].y] = symbol;
    }
}

struct Game prepare_frame(struct Game *game) {
    memset(game->board, ' ', COLS * ROWS * sizeof(char));
    game->board[game->food.x][game->food.y] = game->food.symbol;
    return (*game);
}

struct Game init_board() {
    struct Game game;
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            game.board[i][j] = ' ';
        }
    }

//     init_player(&game.player1);
// //    game.player1.body[0].x = 49;
// //    game.player1.body[0].y = 49;
//     init_player(&game.player2);


 

    generateFood(&game);



    return game;
}
