#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "lib.h"
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include "field.h"
#include "network.h"

#define TIMEOUT 1000


void clearScreen(void) {
    printf("\033[0;0H\033[2J");
    fflush(stdout);
}

void drawCurrentState(struct Game game) {
    int cols;
    int rows;
    tc_get_cols_rows(&cols, &rows);
    tc_move_cursor((cols - 12) / 2, rows / 2);

    printf("%s%s Scores %s %s", TC_YEL, TC_BG_GRN, "7777asd", TC_NRM);
    tc_move_cursor(0, 0);

    for (size_t i = 0; i <= ROWS; ++i) {
        printf("--");
    }

    printf("\n");

    for (int i = 0; i < COLS; i++) {
        for (int j = 0; j < ROWS; j++) {
            if (j == 0) {
                printf("| ");
            }
            printf("%c ", game.board[i][j]);
            if (j == ROWS - 1) {
                printf("|");
            }
        }
        printf("\n");
    }

    for (size_t i = 0; i <= ROWS; ++i) {
        printf("--");
    }
    printf("\n");
}

void exit_game() {
    tc_exit_alt_screen();
    reset_keypress();
    exit(0);
}





int main() {

    srand(time(NULL));

    int sock_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock_fd == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) == -1) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_un serv_addr;
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    strncpy(serv_addr.sun_path, SOCKET_PATH, sizeof(serv_addr.sun_path) - 1);

    if (connect(sock_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1) {
        perror("connect");
        exit(EXIT_FAILURE);
    }

    struct pollfd fds[2];
    memset(&fds, 0, sizeof(fds));
    fds[0].fd = 0;
    fds[0].events = POLLIN; // without blocking
    fds[1].fd = sock_fd;
    fds[1].events = POLLIN;

    //struct Game game = init_board();
    struct Game game;
    int res = (int)recv(sock_fd, &game, sizeof(struct Game), 0);
    if (res == -1) {
        exit(1);
    }



    set_keypress();
    tc_enter_alt_screen();


    char lastInput = 'x';

    while (1) {
        clearScreen();
        drawCurrentState(game);

        int ret = poll(fds, 2, TIMEOUT);
        if (ret == -1) {
            exit_game();
        }

        if (ret > 0) {
            if(fds[1].revents & POLLIN){
                int res = (int)recv(sock_fd, &game, sizeof(game), 0);
                if (res == -1) {
                    perror("recv");
                    exit(EXIT_FAILURE);
                }
                FILE * f = fopen("data.txt", "w");
                fprintf(f, "Data: %c\n", game.food.symbol);
                fclose(f);
                continue;
            }
            char c = (char) getc(stdin);
            if (c == 'q') {

                exit_game();
                break;
            }

            char keys[2];
            keys[1] = c;
            keys[0] = lastInput;
            if (send(fds[1].fd, &keys, 2 * sizeof(char), 0) == -1) {
                perror("send");
                exit_game();
            }
            lastInput = c;

        } else {
            char keys[2];
            keys[0] = lastInput;
            keys[1] = lastInput;
            if (send(fds[1].fd, &keys, 2 * sizeof(char), 0) == -1) {
                perror("send");
                exit_game();
            }
        }
    }


}