#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <poll.h>
#include <arpa/inet.h>
#include <errno.h>
#include "field.h"
#include "network.h"
#include <signal.h>

void sigkill_handler() {
    //printf("KEK");
    remove(SOCKET_PATH);
    exit(1);
}

void print_err() {
    fprintf(stderr, "%s", strerror(errno));
    exit(45);
}

struct Velocities getVelocities(char c) {
    struct Velocities res = {};
    if (c == 'w') {
        res.dx = -1;
        res.dy = 0;
    } else if (c == 'a') {
        res.dx = 0;
        res.dy = -1;
    } else if (c == 's') {
        res.dx = 1;
        res.dy = 0;
    } else if (c == 'd') {
        res.dx = 0;
        res.dy = 1;
    } else {
        res.dx = 0;
        res.dy = 0;
    }

    return res;
}

int move(struct Game *game, struct Player *player, struct Velocities v, char last_button) {
    int res = 0;
    if (player->length > 1) {
        if (player->body[0].x + v.dx == player->body[1].x && player->body[0].y + v.dy == player->body[1].y) {
            v = getVelocities(last_button);
            res = 1;
        }
    }

    player->body[0].x_prev = player->body[0].x;
    player->body[0].y_prev = player->body[0].y;

    player->body[0].x += v.dx;
    player->body[0].y += v.dy;

    if (player->body[0].x >= COLS) {
        player->body[0].x = 0;
    }

    if (player->body[0].x < 0) {
        player->body[0].x = COLS - 1;
    }

    if (player->body[0].y < 0) {
        player->body[0].y = ROWS - 1;
    }

    if (player->body[0].y >= ROWS) {
        player->body[0].y = 0;
    }


    for (size_t i = 1; i < player->length; i++) {
        player->body[i].x_prev = player->body[i].x;
        player->body[i].y_prev = player->body[i].y;

        player->body[i].x = player->body[i - 1].x_prev;
        player->body[i].y = player->body[i - 1].y_prev;

    }

    for (size_t i = 1; i < player->length; i++) {
        if (player->body[0].x == player->body[i].x && player->body[0].y == player->body[i].y) {
            printf("BOOOM");
            remove(SOCKET_PATH);
            exit(199);
        }
    }

    if (player->body[0].x == game->food.x && player->body[0].y == game->food.y) {
        player->body[player->length].x = player->body[player->length - 1].x_prev;
        player->body[player->length].y = player->body[player->length - 1].y_prev;
        player->length++;
        update_food_cords(game);
    }

    return res;
}


int handleButton(struct Game *game, struct Player *player, char c, char last_move) {
//    FILE * tmp = fopen("tmp.txt", "w+");
//    fprintf(tmp, "%c %c", c, last_move);
//    fclose(tmp);
    for (size_t i = 0; i < player->length; i++) {
        printf("(%d %d) ", player->body[i].x, player->body[i].y);
    }
    printf("\n");
    int res;
    res = move(game, player, getVelocities(c), last_move);

    return res;
}

void send_updated_game_to_clients(struct Game *game, const struct pollfd *fds, int nfds, struct Player players[2]) {
    prepare_frame(game);
    for(size_t  i = 0; i < nfds - 1; i++){
        update_field_player(game, players[i]);
    }


    for (int j = 1; j <= nfds - 1; j++) {
        if (send(fds[j].fd, game, sizeof(struct Game), 0) == -1) {
            perror("send");
            continue;
        }
    }
}

int main() {
    struct Player players[MAX_CLIENTS];
    signal(SIGKILL, &sigkill_handler);
    signal(SIGINT, &sigkill_handler);
    struct Game game = init_board();
    int sock_fd = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if (sock_fd == -1) {
        print_err();
    }

    if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &(int) {1}, sizeof(int)) < 0) {
        print_err();
    }


    if (fcntl(sock_fd, F_SETFL, O_NONBLOCK) == -1) {
        perror("fcntl");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_un serv_addr;


    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    strncpy(serv_addr.sun_path, SOCKET_PATH, sizeof(serv_addr.sun_path) - 1);

    if (bind(sock_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) == -1) {
        print_err();
    }

    if (listen(sock_fd, MAX_CLIENTS) == -1) {
        print_err();
    }

    struct pollfd fds[MAX_CLIENTS + 1];

    fds[0].fd = sock_fd;
    fds[0].events = POLLIN;

    int nfds = 1;


    while (1) {
        int ret = poll(fds, nfds, 1000);
        if (ret == -1) {
            print_err();
        }

        if (fds[0].revents & POLLIN) {
            // Accept incoming connection
            struct sockaddr_in client_addr;

            memset(&client_addr, 0, sizeof(client_addr));
            socklen_t addr_len = sizeof(client_addr);
            int client_fd = accept(sock_fd, (struct sockaddr *) &client_addr, &addr_len);
            if (client_fd == -1) {
                print_err();
            }

            // Add new client_fd to pollfd array
            if (nfds == MAX_CLIENTS + 1) {
                fprintf(stderr, "Too many clients\n");
                close(client_fd);
                exit(255);
            } else {
                fds[nfds].fd = client_fd;
                fds[nfds].events = POLLIN;
                fds[nfds].revents = 0;
                printf("New client connected \n");
                struct Player player;
                init_player(&player, nfds);
                players[nfds - 1] = player;

                nfds++;
                printf("NFDS %d\n", nfds);
                send_updated_game_to_clients(&game, fds, nfds, players);
            }
        }


        // Check for events on client_fd
        for (size_t i = 1; i < nfds; i++) {
            if (fds[i].revents & POLLIN) {
                // Receive data from client
                char inputs[2];
                ret = (int) recv(fds[i].fd, &inputs, 2 * sizeof(char), 0);
                if (ret == -1) {
                    perror("recv");
                    continue;
                } else if (ret == 0) {
                    printf("Client disconnected\n");
                    close(fds[i].fd);

                    // Remove client_fd from pollfd array
                    fds[i] = fds[nfds - 1];
                    nfds--;
                    i--;
                    continue;
                }


                printf("HANDLE %zu |||||| Last %c Now %c\n", i, inputs[0], inputs[1]);

                int res = handleButton(&game, &players[i - 1], inputs[1], inputs[0]);
            }

        }
        send_updated_game_to_clients(&game, fds, nfds, players);
    }
}